# UserApi

## Historique

* Mise en place de l'environnement Docker avec un Php-Apache et un container MySQL
* Démarrage de l'environnement `docker-compose up --build -d`
* Lancement d'une console dans le container Php-Apache `docker exec -it userapi_phpapache bash`

* Installation de Symfony `composer create-project symfony/skeleton userapi` et fusion des dossiers

* Installation d'Apache Pack : `composer require symfony/apache-pack`
* Installation de Doctrine et de son maker en dev : `composer require symfony/orm-pack && composer require symfony/maker-bundle --dev`
